<?php

namespace App\Http\Controllers\Api;

use App\Api\Weather\Cities;
use App\Api\Weather\ProviderBroker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WeatherController extends Controller
{
    public function getByCity($city, $format = 'json'){
        $cityObject = Cities::getCityByName($city);

        $apiProvider = ProviderBroker::getWeatherApiSource();
        $weatherApiResponse = $apiProvider->getWeatherByPoint($cityObject['long'], $cityObject['lat']);

        if($format == 'json'){
            return response($weatherApiResponse->getJson(), 200, [
                'Content-Type' => 'application/json'
            ]);

        }elseif($format == 'rich-json'){
            return response($weatherApiResponse->getRichJson(), 200, [
                'Content-Type' => 'application/json'
            ]);

        }else{
            return response($weatherApiResponse->getXml(), 200, [
                'Content-Type' => 'application/xml'
            ]);
        }
    }
}
