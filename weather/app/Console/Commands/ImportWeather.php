<?php

namespace App\Console\Commands;

use App\Api\Weather\Cities;
use App\Api\Weather\ProviderBroker;
use Illuminate\Console\Command;

class ImportWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:weather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import weather from API source';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $apiProvider = ProviderBroker::getWeatherApiSource();


        $citiesToGetWeather = Cities::getCities();
        foreach ($citiesToGetWeather as $city){

           // work on $city to save to DB
            $weatherApiResponse = $apiProvider->getWeatherByPoint($city['long'], $city['lat']);
        }

    }
}
