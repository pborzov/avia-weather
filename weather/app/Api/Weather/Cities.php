<?php

namespace App\Api\Weather;

class Cities
{
    private static $cities = [
        [
            'name' => 'Москва',
            'long' => '55.75396',
            'lat'  => '37.620393',
        ],

        [
            'name' => 'Симферополь',
            'long' => 44.948060,
            'lat'  => 34.104170,
        ],

        [
            'name' => 'Севастополь',
            'long' => '44.600000',
            'lat'  => '33.533330',
        ],
    ];


    public static function getCities(){
        return self::$cities;
    }

    public static function getCityByName($cityName){
        foreach (self::$cities as $city){
            if($city['name'] == $cityName){
                return $city;
            }
        }


        throw new \Exception('Not supported city');
    }
}
