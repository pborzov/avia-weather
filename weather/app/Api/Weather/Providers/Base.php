<?php

namespace App\Api\Weather\Providers;
use App\Api\Weather\Entity;
use GuzzleHttp\Client;

abstract class Base
{
    const QUERY_METHOD_GET  = 'get';
    const QUERY_METHOD_POST = 'post';

    //Is used to mark record in DB
    public $providerId = 'base';

    public function runQuery($authHeader, $url, $params = [], $method = self::QUERY_METHOD_GET ){
        if ($authHeader && !is_array($authHeader)){
            throw new \Exception("Malformed header param");
        }
        if ($params && !is_array($params)){
            throw new \Exception("Malformed params");
        }


        $client = new Client(['headers' => $authHeader]);

        if($method == self::QUERY_METHOD_POST){
            $response  = $client->post($url,  ['body' => $params] );
        }else{
            $response = $client->get($url, ['query' => $params]);

        }



        return $this->decodeResponse( $response->getBody()->getContents() );
    }

    public abstract function decodeResponse(string $responseBody);

    /**
     * @param string $long
     * @param string $lat
     * @return Entity
     * @throws \Exception
     */
    public abstract function getWeatherByPoint(string $long, string $lat);
}
