<?php

namespace App\Api\Weather\Providers;

use App\Api\Weather\Entity;
use Carbon\Carbon;

class Yandex extends Base
{

    public function getWeatherByPoint(string $long, string $lat)
    {
        $weatherDataYandexFormat = $this->runQuery($this->getAuthHeader(), $this->getUrlByMethod('forecast'), [
            'lat'   => $lat,
            'lon'   => $long,
            //'extra' => 'true',
        ]);

        $weatherDataYandexFormat = $weatherDataYandexFormat['fact'];

        $weatherEntity = new Entity();
        $weatherEntity->setDate(Carbon::now());
        $weatherEntity->setHumidity($weatherDataYandexFormat['humidity']);
        $weatherEntity->setWindDirection($weatherDataYandexFormat['wind_dir']);
        $weatherEntity->setWindPower($weatherDataYandexFormat['wind_speed']);
        $weatherEntity->setTemperature($weatherDataYandexFormat['temp']);

        return $weatherEntity;
    }

    public function decodeResponse(string $responseBody)
    {
        return json_decode($responseBody, true);
    }

    private function getAuthHeader(){
        return [
            'X-Yandex-API-Key' => config('api.yandex.token'),
        ];
    }

    private function getUrlByMethod($method = 'forecast'){
        return 'https://api.weather.yandex.ru/v1/' . $method;
    }
}
