<?php

namespace App\Api\Weather;

use App\Api\Weather\Providers\Base;

class ProviderBroker
{

    /**
     * @return Base
     */
    public static function getWeatherApiSource(){
        // Решение о том как и какое именно провадер погодных данных должен приниматься исходя из бизнесс-логики
        // В данном случае это просто прописано в конфиге
        $providerClassName = config('api.weather_provider');
        $provider = new $providerClassName;
        return $provider;
    }
}
