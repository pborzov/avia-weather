<?php

namespace App\Api\Weather;

class Entity
{


    public $date          = '';
    public $windPower     = '';
    public $windDirection = '';
    public $temperature   = '';
    public $humidity      = '';

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @param string $windPower
     */
    public function setWindPower(string $windPower): void
    {
        $this->windPower = $windPower;
    }

    /**
     * @param string $windDirection
     */
    public function setWindDirection(string $windDirection): void
    {
        $this->windDirection = $windDirection;
    }

    /**
     * @param string $temperature
     */
    public function setTemperature(string $temperature): void
    {
        $this->temperature = $temperature;
    }

    /**
     * @param string $humidity
     */
    public function setHumidity(string $humidity): void
    {
        $this->humidity = $humidity;
    }

    public function getRichJson(){
        $prepareExportData = [
            'date'          => $this->date,
            'temperature'   => $this->temperature,
            'windDirection' => $this->windDirection,
            'windPower'     => $this->windPower,
            'humidity'      => $this->humidity,
        ];

        return json_encode($prepareExportData);
    }

    public function getJson(){
        $prepareExportData = [
            $this->date,
            $this->temperature,
            $this->windDirection,
            $this->windPower,
            $this->humidity,
        ];

        return json_encode($prepareExportData);
    }

    public function getXml(){
        $prepareExportData = [
            'date'          => $this->date,
            'windPower'     => $this->windPower,
            'temperature'   => $this->temperature,
            'windDirection' => $this->windDirection,
            'humidity'      => $this->humidity,
        ];

        return $this->arrayToXml($prepareExportData);
    }


    private function arrayToXml($array, $rootElement = null, $xml = null) {
        $_xml = $xml;
        if ($_xml === null) {
            $_xml = new \SimpleXMLElement($rootElement !== null ? $rootElement : '<root/>');
        }

        foreach ($array as $k => $v) {
            if (is_array($v)) {
                $this->arrayToXml($v, $k, $_xml->addChild($k));
            }

            else {
                $_xml->addChild($k, $v);
            }
        }
        return $_xml->asXML();
    }

}
